import React, { Component } from "react";
import TodoItem from "./TodoItem";

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {/* list is equivalent to array [1, 2, 3, 4, 5] */}
          {/* its converting an array of data items (todo objects) -> array of components */}
          {this.props.todos.map(todo => (
            <TodoItem 
              key={todo.id}
              title={todo.title} 
              completed={todo.completed}
              handleToggleTodo={event =>
                this.props.handleToggleTodo(todo.id)
              } 
              handleDeleteTodo={event =>
                this.props.handleDeleteTodo(todo.id)
              }
              />
          ))}
        </ul>
      </section>
    );
  }
}

export default TodoList;