import React, { Component } from "react";
import { Route, NavLink } from "react-router-dom";
import "./index.css";
import todosList from "./todos.json";
// import TodoItem from "./TodoItem";
import TodoList from "./TodoList";

class App extends Component {
  state = {
    todos: todosList
  };

  handleToggleTodo = (todoIdToToggle) => {
    // create a copy of data to update
    const newTodos = this.state.todos.slice();
    // modify the copy
    const newnewTodos = newTodos.map(todo => {
      // find todo to modoify
      if (todo.id === todoIdToToggle) {
        // change completed value from false to true
       todo.completed = !todo.completed;
      }
      return todo;
    });
    // overwrite the original with copy
    this.setState({ todos: newnewTodos})
  };

  handleCreateTodo = (event) => {
    if (event.key === "Enter") {
      // how to create a new todo
      const newTodo = {
        userId: 1,
        id: Math.floor(Math.random() * 100000),
        title: event.target.value,
        completed: false 
      };
      
      // how do i update component state so it has new todo

      // the immutability pattern
      // create copy of data you want to update
      const newTodos = this.state.todos.slice();
      // modify the copy
      newTodos.push(newTodo);
      // overwrite orginal with the copy
      // this.setState tells react to do a re-render
      this.setState({ todos: newTodos })
      event.target.value = "";
    }
  }

  handleDeleteTodo = (todoIdToDelete) => {
    // Remove todo when "X" button is selected
    // create a copy of data to update
    const newTodos = this.state.todos.filter(todo =>
      // find todo to modify
      (todo.id !== todoIdToDelete)
    );
    // overwrite the original with copy
    this.setState({ todos: newTodos })
  }

  handleClearCompletedTodos = () => {
    // Remove all completed todos (todos with checkmarks)
    // create a copy of data to update
    const newTodos = this.state.todos.filter(todo => {
      // find todo to modoify
      if (todo.completed === true) {
        // change completed value from false to true
       return false;
      }
      return true;
    });
    // overwrite the original with copy
    this.setState({ todos: newTodos })
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            onKeyDown={this.handleCreateTodo}
            autoFocus
          />
        </header>
        <Route
          exact
          path="/"
          render={() => (
            <TodoList 
              todos={this.state.todos}
              handleToggleTodo={this.handleToggleTodo}
              handleDeleteTodo={this.handleDeleteTodo}
            />
          )}
        />
        <Route
          exact
          path="/active"
          render={() => (
            <TodoList 
              todos={this.state.todos.filter(todo => todo.completed === false)}
              handleToggleTodo={this.handleToggleTodo}
              handleDeleteTodo={this.handleDeleteTodo}
            />
          )}
        />
        <Route
          exact
          path="/completed"
          render={() => (
            <TodoList 
              todos={this.state.todos.filter(todo => todo.completed === true)}
              handleToggleTodo={this.handleToggleTodo}
              handleDeleteTodo={this.handleDeleteTodo}
            />
          )}
        />
        <footer className="footer">
          {/* <!-- This should be `0 items left` by default --> */}
          <span className="todo-count">
            <strong>
              {
                this.state.todos.filter(todo => {
                  if (todo.completed === false) {
                    return true;
                  }
                  return false;
                }).length
              }
            </strong>{" "}
            item(s) left
            </span>
          <ul className="filters">
            <li>
              <NavLink exact to="/" activeClassName="selected">All</NavLink>
            </li>
            <li>
              <NavLink exact to="/active" activeClassName="selected">Active</NavLink>
            </li>
            <li>
              <NavLink exact to="/completed" activeClassName="selected">Completed</NavLink>
            </li>
          </ul>
          <button className="clear-completed" onClick={ this.handleClearCompletedTodos }>Clear completed</button>
        </footer>
      </section>
    );
  }
}

export default App;
